const AWS = require('aws-sdk')

// Set the region 
var awsRegion = process.env.region;
AWS.config.update({region: awsRegion});

var bucketName = process.env.bucket_name;

// Create the parameters for calling listObjects
var bucketParams = {
    Bucket : bucketName,
};

const s3 = new AWS.S3({
    apiVersion: '2006-03-01'
});

exports.handler = (event, context, callback) => {
    console.log('ok:start');
    // Call S3 to obtain a list of the objects in the bucket
    s3.listObjects(bucketParams, function(err, data) {
        console.log('------------------------------ start');

        var key = '2019/07/09/21/dev_loan-is-funded-firehose-4-2019-07-09-21-37-35-623f189e-d2da-4a3f-93b6-14744a23c5e6';
        var params = {Bucket: bucketName, Key: key};
        const stream = s3.getObject(params, function(err, data) {
            if (!err) {
                var lines = data.Body.toString('utf8').split('\n');
                console.log(lines[0]);
                var emailJson = JSON.parse(lines[0]);
                console.log('emailJson: ',emailJson.mail.source);
                console.log('eventType: ',emailJson.eventType);
            } else {
                console.log(err);
            }
        });
        
        console.log('------------------------------ end');
        if (err) {
            console.log("Error", err);
        } else {
            // console.log("Success", data);
        }
    });
    console.log('ok:end');
};