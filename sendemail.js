// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');

// Set the region 
var aws_region = process.env.region;
AWS.config.update({region: aws_region});

// Create sendEmail params 
exports.handler = (event, context, callback) => {
    var subject = process.env.subject;
    var configSetName = process.env.ses_configuration_set;
    var fromAddress = process.env.from_address;
    // var emailList = process.env.email_list;
    var toAddresses = ['davit.hakobyan@clearlane.com']; //emailList.split('|');

    var params = {
        ConfigurationSetName: configSetName,
        Source: fromAddress,
        Destination: { 
            ToAddresses: toAddresses,
        },
        Message: { 
            Subject: {
                Charset: "UTF-8",
                Data: subject
            },
            Body: { 
                Html: {
                    Charset:"UTF-8",
                    Data: messageBodyHTML
                },
            }
        }
    };
    
    // Create the promise and SES service object
    var sendPromise = new AWS.SES({apiVersion: '2010-12-01'}).sendEmail(params).promise();
    
    // Handle promise's fulfilled/rejected states
    sendPromise.then(function(data) {
        callback(null, 'Email has been sent successfuly.');
        console.log(data.MessageId);
    }).catch(function(err) {
        callback(null, 'Faild to send an Email.');
        console.error(err, err.stack);
    });
};

var messageBodyHTML = `<html><head>AWS Lambda created by Davit Hakobyan</head>
<body>
  <h1>AWS Lambda created by Davit Hakobyan</h1>
  <p>This email was send by AWS Lambda on a a defined schedule. </p>
    <br/><a ses:tags="lender:creditkarma;" href = "https://www.creditkarma.com/reviews/auto-loan/single/id/Blue-Harbor-Auto-Loans">Clearlane on Credit Karma</a>
</body>
</html>`;

// <br/><a ses:tags="lander:lendingtree;" href = "https://www.lendingtree.com/loan-companies/clearlane-reviews-20425">Clearlane on Lending Tree</a>